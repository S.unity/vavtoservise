var selct1 = false;
$(document).ready(function() {
    $(document).ready(function() {
        $('.dates').dateDropper({
            lang: 'ru',
            readonly: '',
            format: 'd-m-Y'
        });
        for (let item of document.querySelectorAll(".dates")) {
            if (item.getAttribute("disabled") === null) {
                item.removeAttribute("readonly")
            }
        }
    });
    //Выбор brand из селекта
    $('.js-example-brend').change(function () {
        $('.js-example-model').prop('disabled',false);
    });
    $('.js-example-brend').select2({
        placeholder:"Выберите Марку",
        ajax: {
            delay: 250,
            type: 'POST',
            data: function (params) {
                params.q=params.term;
                return params;
            },
            url: '/ajax/car/brend/',
            dataType: 'json',
            processResults: function (data) {
                //alert(data);
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            }
        }
    });

    $('.js-example-model').select2({
        placeholder:"Выберите Модель",
        ajax: {
            delay: 250,
            type: 'POST',
            data: function (params) {
                var id_model = this.parents('.parent').find('.js-example-brend').val();

                params.brend_id=id_model;
                params.q=params.term;
                return params;
            },
            url: '/ajax/car/model/',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            }
        }
    });
  $('[data-toggle="tooltip"]').tooltip();   
});