"use strict";

class AutoServiceMap {

    constructor(canvas) {
        this.canvas = canvas;

        this.markers = [];
        this.infoWindows = [];

        this.defaultZoom = 3;

        this.defaultCenter = {
            lat: 59.9992618,
            lng: 30.3648466
        };
    }

    /**
     * call-me when api is ready
     */
    initMap() {
        this.map = new google.maps.Map(this.canvas, {
            zoom: this.defaultZoom,
            center: this.defaultCenter
        });
    }


    removeMarkerBySID(service_id) {
        return;
    }

    clearMarkers() {
        for (let item of this.markers) {
            item.setMap();
        }
        this.markers.length = 0;
    }

    addAutoServiceMarker(service) {
        let marker = new google.maps.Marker({
            position: {"lat": service.lat, "lng":service.lng},
            animation: google.maps.Animation.DROP,
            title: service.name
        });

        marker["_service"] = service;
        marker.addListener('click', function () {
            this._this.onMarkerClick(this.marker);
        }.bind({"_this":this,"marker":marker}));

        marker.setMap(this.map);
        this.markers.push(marker);
    }

    addAutoServicesMarkers(array) {
        for (let item of array) {
            this.addAutoServiceMarker(item);
        }
    }

    closeInfoWindows() {
        for (let item of this.infoWindows) {
            item.close();
        }
        this.infoWindows.length = 0;
    }

    setMarkerInfoWindow(marker) {
        this.closeInfoWindows();
        let contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h3 id="firstHeading" class="firstHeading">'+marker._service.name+'</h3>'+
            '<div id="bodyContent">'+
            '<p>'+marker._service.phone+'</p>'+
            '</div>'+
            '</div>';

        let infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        infowindow.open(this.map, marker);
        this.infoWindows.push(infowindow);
    }

    onMarkerClick(marker) {}
}