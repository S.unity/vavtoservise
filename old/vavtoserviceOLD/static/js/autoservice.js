"use strict";

class City {
    constructor(obj) {
        this._id = parseInt(obj["id"]);
        this._name = obj["name"];
        this._phone = obj["phone"];
    }
    get id() {
        return this._id;
    }
    get name() {
        return this._name;
    }
    get phone() {
        return this._phone
    }
}

class AutoService {
    constructor(data, prop, additions) {
        this._data = data;
        this._prop = prop;
        this._additions = additions;
    }

    get data() {
        return this._data;
    }
    get prop() {
        return this._prop;
    }
    get additions() {
        return this._additions;
    }

    get lat() {
        return parseInt(this._data["lat"]);
    }
    get lng() {
        return parseInt(this._data["lng"]);
    }

    get description() {
        return this._data["description"];
    }
    get name() {
        return this._data["name"];
    }
    get phone() {
        return this._data["phone"];
    }
    get id() {
        return parseInt(this._data["id"]);
    }
    get city_id() {
        return parseInt(this._data["city_id"]);
    }

    getProp(name) {
        return this._prop[name];
    }
}

//**

class AutoModelStore {
    getSelect2Ajax(getter) {
        return {
            delay: 250,
            type: 'POST',
            data: function (params) {
                params.brend_id = getter.mark_id;
                params.q = params.term;
                return params;
            },
            url: '/ajax/car/model/',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            }
        };
    }
    getSelect2Ajaxs(getter) {
        return {
            delay: 250,
            type: 'POST',
            data: function (params) {
                params.brend_id = getter.marks_id;
                params.q = params.term;
                return params;
            },
            url: '/ajax/car/model/',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            }
        };
    }
}

class AutoMarkStore {
    getSelect2Ajax() {
        return {
            delay: 250,
            type: 'POST',
            url: '/ajax/car/brend/',
            dataType: 'json',
            processResults: function (data) {
                //  All cars mark
                let res = $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    };
                });
                return {
                    results: res,
                };
            }
        }
    }
}

class CategoryStore {
    getParentSelect2Ajax() {
        return {
            url: '/ajax/cat/allParent/',
            dataType: "json",
            data: function (param) {
                param.q = param.term;
                return param;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.title,
                            id: item.id
                        };
                    })
                }
            }
        }
    }

    getChildSelect2Ajax(id_getter) {
        return {
            url: '/ajax/cat/allChild/',
            dataType: "json",
            data: function (param) {
                if (id_getter && id_getter.cat_parent_id) {
                    param.parent_id = id_getter.cat_parent_id;
                }
                param.q = param.term;
                return param;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.title,
                            id: item.id
                        };
                    })
                }
            }
        }
    }

}

class CityStore {
    getCityByIpAsync(callback, ip="") {
        $.ajax({
            dataType: "json",
            url: "/ajax/geo/getByIp/",
            data: {"ip": ip},
            success: function (data) {
                callback(new City(data));
            }
        });
    }

    getSelect2Ajax(page_lang="ru") {
        return {
            delay: 250,
            type: 'POST',
            url: '/ajax/geo/getAllCity/',
            dataType: 'json',
            data: function (params) {
                params.lang=page_lang;
                params.q = params.term;
                return params;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.text,
                            id: item.id
                        };
                    })
                };
            }
        };
    }
}

class AutoServiceStore {

    getAllAsync(page=1) {

    }

    getSuggestionsAsync(callback, city_id=null, mark_id=null, cat_id=null) {
        let data = {};
        if (city_id) {
            data["city_id"] = city_id
        }
        if (mark_id) {
            data["mark_id"] = mark_id;
        }
        if (cat_id) {
            data["cat_id"] = cat_id;
        }
        $.ajax({
            dataType: "json",
            url: "/ajax/service/suggestion/",
            data: data,
            success: function (data) {
                let result = [];
                for (let item of data) {
                    result.push(new AutoService(item["data"], item["prop"], item["additions"]));
                }
                callback(result);
            }
        });
    }
    getAllService(city_id=5054) {
        return {
            delay: 250,
            url: '/ajax/service/getList/',
            dataType: 'json',
            data: function (params) {
                params.city_id=city_id;
                params.q = params.term;
                return params;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.text,
                            id: item.id
                        };
                    })
                };
            }
        };
    }
    getAllPartner() {
        return {
            url: '/ajax/service/allPartner/',
            dataType: "json",
            data: function (param) {
                param.q = param.term;
                return param;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.title,
                            id: item.id
                        };
                    })
                }
            }
        }
    }

    getSuggestionsSelect2Ajax(getter) {
        return {
            url: '/ajax/service/suggestion/',
            dataType: "json",
            data: function(param){
                param["city_id"] = getter.city_id;
                param["mark_id"] = getter.mark_id;
                param["cat_id"] = getter.cat_id;
                return param;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.data.name,
                            id: item.data.id
                        };
                    })
                }
            }
        }
    }
}
