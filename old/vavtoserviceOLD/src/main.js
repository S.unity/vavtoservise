import Vue from 'vue'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'
import VueMeta from 'vue-meta'
import VueCookie from 'vue-cookie'
import router from './router'
import headerBlock from './components/header'
import footerBlock from './components/footer'
import formSokr from './components/blocks/index-sokr'
import osago from './components/blocks/osago'
import kasko from './components/blocks/kasko'
import texosmotr from './components/blocks/texosmotr'
import reviews from './components/blocks/Reviews'
import serveviews from './components/blocks/Serveviews'
import articles from './components/blocks/Articles'
import BootstrapVue from 'bootstrap-vue'
import $ from 'jquery'
import axios from '@/router/http'
import Inputmask from 'inputmask'
Vue.config.productionTip = false

Sentry.init({
  dsn: 'https://ddd815209c8c4c339bc83aea2f4983c6@sentry.io/1577128',
  integrations: [new Integrations.Vue({Vue, attachProps: true})]
})
Vue.use(VueCookie)
Vue.use(BootstrapVue)
Vue.use(VueMeta, {refreshOnceOnNavigation: true})
Vue.component('header-block', headerBlock)
Vue.component('footer-block', footerBlock)
Vue.component('reviews', reviews)
Vue.component('serveviews', serveviews)
Vue.component('articles', articles)
Vue.component('index-form', formSokr)
Vue.component('osago-form', osago)
Vue.component('kasko-form', kasko)
Vue.component('texosmotr-form', texosmotr)
Vue.component('vnode-injector', {
  functional: true,
  props: ['nodes'],
  render (h, {props}) {
    return props.nodes
  }
})
Vue.directive('click-outside', {
  bind (el, binding) {
    el.addEventListener('click', e => e.stopPropagation())
    document.body.addEventListener('click', binding.value)
  },
  unbind (el, binding) {
    document.body.removeEventListener('click', binding.value)
  }
})

new Vue({
  el: '#app',
  router,
  metaInfo: {
    title: 'все автосервисы города на одном сайте. Честная стоимость ремонта и запись в сервис без очереди.',
    titleTemplate: '«В-автосервис» — %s',
    description: 'description',
    htmlAttrs: {
      lang: 'ru'
    }
  },
  data: {
    city: {
      id: null,
      name: null,
      email: null,
      phone: null,
      lang: null,
      bonus: null,
      valute: null
    },
    user: {
      uid: null,
      mark: null,
      model: null,
      year: null,
      vin: null,
      district: null,
      phone: null,
      email: null,
      name: null,
      push: null,
      user: null
    },
    auth: {
      id: null,
      city_id: null,
      name: null,
      famaliname: null,
      patronymic: null,
      password: null,
      email: null,
      status: null,
      date: null,
      tel: null,
      purse: null,
      bonus: null,
      ref: null,
      role: null,
      created_at: null,
      updated_at: null
    },
    service: {
      id: null,
      city_id: null,
      description: null,
      name: null,
      phone: null,
      email: null,
      uid: null,
      coord: null,
      address: null,
      status: null,
      bonus: null,
      created_at: null,
      updated_at: null,
      deleted_at: null,
      profit: null,
      fix: null,
      property: {
        photo: '',
        address: '',
        auto: '',
        bank_name: '',
        bik: '',
        birthday: '',
        bonus: '',
        bonusin: '',
        card: '',
        checking_account: '',
        date_dogovor: '',
        dil: '',
        district: '',
        dogovor: '',
        eva: '',
        full_legal_name: '',
        inn: '',
        kpp: '',
        legal_address: '',
        logo: '',
        ogrn: '',
        parking: '',
        top10: '',
        rating: '',
        rest: '',
        telegram: '',
        tv: '',
        validate: '',
        viber: '',
        video: '',
        whatsapp: '',
        wifi: '',
        zap: ''
      },
      reviews: [],
      fixprices: [],
      to_cat: []
    },
    leads: []
  },
  created () {
    var city = this.$cookie.get('city')
    var uid = this.$cookie.get('uid')
    if (uid) this.user.uid = uid
    var hostname = window.location.hostname
    if (hostname === 'localhost') hostname = 'в-автосервис.рф'
    hostname = hostname.split('.')
    var hostnameforcookie
    if (hostname.length === 3) hostnameforcookie = hostname[1] + '.' + hostname[2]
    else hostnameforcookie = hostname[0] + '.' + hostname[1]
    // hostnameforcookie = 'localhost'
    // hostname = 'localhost'
    if (city) city = JSON.parse(city)
    if (city && city.name && city.id) {
      this.city = city
      if (hostname.length === 3) {
        if (this.city.name !== hostname[0]) {
          axios.get('https://crm.v-avtoservice.com/list/site-get-city-by-name?name=' + hostname[0] + '&url=' + hostnameforcookie).then(response => {
            this.city = response.data
            this.$cookie.set('city', JSON.stringify(response.data), {expires: '1Y', domain: hostnameforcookie})
            // location.reload()
          })
        } else {
          axios.get('https://crm.v-avtoservice.com/list/site-get-city-by-name?name=' + this.city.name + '&url=' + hostnameforcookie).then(response => {
            this.city = response.data
            this.$cookie.set('city', JSON.stringify(response.data), {expires: '1Y', domain: hostnameforcookie})
            // location.reload()
          })
        }
      } else {
        axios.get('https://crm.v-avtoservice.com/list/site-get-city-by-name?name=' + this.city.name + '&url=' + hostnameforcookie).then(response => {
          this.city = response.data
          this.$cookie.set('city', JSON.stringify(response.data), {expires: '1Y', domain: hostnameforcookie})
          // location.reload()
        })
      }
    } else {
      if (hostname.length === 3) {
        axios.get('https://crm.v-avtoservice.com/list/site-get-city-by-name?name=' + hostname[0] + '&url=' + hostnameforcookie).then(response => {
          this.city = response.data
          this.$cookie.set('city', JSON.stringify(response.data), {expires: '1Y', domain: hostnameforcookie})
          // location.reload()
        })
      } else {
        axios.get('https://crm.v-avtoservice.com/list/site-get-city-by-url?url=' + hostnameforcookie).then(response => {
          this.city = response.data
          this.$cookie.set('city', JSON.stringify(response.data), {expires: '1Y', domain: hostnameforcookie})
          // location.reload()
        })
      }
    }
    if (this.city.name) {
      if (!this.user.uid) {
        let invite = new URL(window.location.href).searchParams.get('invite')
        axios.get('https://crm.v-avtoservice.com/list/site-create-uid?city=' + this.city.name + '&invite=' + invite).then(response => {
          this.user.uid = response.data.uid
          this.user.invite = invite
          this.$cookie.set('uid', this.user.uid, {expires: '1Y', domain: hostnameforcookie})
        })
      } else {
        axios.get('https://crm.v-avtoservice.com/list/site-get-uid?uid=' + this.user.uid).then(response => {
          this.user.uid = response.data.uid
          this.user.mark = response.data.mark
          this.user.model = response.data.model
          this.user.year = response.data.year
          this.user.vin = response.data.vin
          this.user.district = response.data.district
          this.user.phone = response.data.phone
          this.user.email = response.data.email
          this.user.name = response.data.name
          this.user.push = response.data.push
          this.user.user = response.data.user
          this.user.invite = response.data.invite
          axios.get('https://crm.v-avtoservice.com/list/site-get-auth?uid=' + this.user.uid).then(response => {
            this.auth = response.data
            if (this.auth.role === 1) {
              axios.get('https://crm.v-avtoservice.com/list/site-get-auth-service?uid=' + this.user.uid).then(response => {
                this.service = response.data
              })
              this.serviceLeads()
              setInterval(() => this.serviceLeads(), 300 * 1000)
            } else {
              this.clientLeads()
              setInterval(() => this.clientLeads(), 300 * 1000)
            }
          })
        })
      }
    }
    updateView(window.location.pathname)
    Inputmask().mask(document.querySelectorAll('input'))
  },
  mounted () {
    updateView(window.location.pathname)
    Inputmask().mask(document.querySelectorAll('input'))
  },
  methods: {
    serviceLeads () {
      axios.get('https://crm.v-avtoservice.com/list/service-get-leads?uid=' + this.user.uid).then(response => {
        this.leads = response.data
      })
    },
    clientLeads () {
      axios.get('https://crm.v-avtoservice.com/list/client-get-leads?uid=' + this.user.uid).then(response => {
        this.leads = response.data
      })
    }
  },
  watch: {
    'user' () {
      console.log('user')
    },
    'auth' () {
      console.log('auth')
    },
    'city.name' (val) {
      console.log(val)
      if (val === 'Барнаул') {
        let recaptchaScript = document.createElement('script')
        recaptchaScript.setAttribute('src', 'https://w.tb.ru/open-messenger/widget?wId=W-DC63BD2236104DC4829D0A892D6F4278')
        document.head.appendChild(recaptchaScript)
      }
    },
    $route (to, from) {
      updateView(to.path)
      setTimeout(() => { Inputmask().mask(document.querySelectorAll('input')) }, 400)
    }
  }
})
function updateView (link, timeot = 0) {
  if (link === '/sale-franchise/' || link === '/fran/' || link === '/sale-franchise-uk/' || link === '/sale-franchise-by/' || link === '/sale-investment/' || link === '/sale-franchise' || link === '/fran' || link === '/sale-franchise-uk' || link === '/sale-franchise-by' || link === '/sale-investment') {
    $('#style').attr('href', '')
    $('#footer').hide()
    $('#header').hide()
  } else {
    $('#style').attr('href', '/static/css/general/style.css')
    $('#footer').show()
    $('#header').show()
  }
  if (timeot === 0) {
    setTimeout(updateView(link, 1), 500)
  }
}
