import Vue from 'vue'
import axios from 'axios'
import Loading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'
Vue.use(Loading)

// create a new axios instance
const instance = axios.create({
  baseURL: '/api'
})

let loader = []
// before a request is made start the nprogress
instance.interceptors.request.use(config => {
  if (loader[config.url] !== undefined) loader[config.url].hide()
  loader[config.url] = Vue.$loading.show({
    loader: 'dots',
    opacity: 0.8,
    backgroundColor: 'transparent',
    color: '#4A80C6',
    isFullPage: true
  })
  return config
})

// before a response is returned stop nprogress
instance.interceptors.response.use(response => {
  loader[response.config.url].hide()
  return response
})

export default instance
