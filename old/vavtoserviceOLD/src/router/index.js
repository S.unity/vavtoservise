import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import CheckLink from '@/components/CheckLink'
import Stocks from '@/components/Stocks'
import AllServices from '@/components/AllServices'
import AllCarServices from '@/components/AllCarServices'
import Battery from '@/components/Battery'
import Blog from '@/components/Blog'
import Bonus from '@/components/Bonus'
import AllMarks from '@/components/AllMarks'
import ConnectService from '@/components/ConnectService'
import Connection from '@/components/Connection'
import Contacts from '@/components/Contacts'
import Kresla from '@/components/Kresla'
import SaleFranchise from '@/components/SaleFranchise'
import Akb from '@/components/Akb'
import News from '@/components/News'
import SaleBusiness from '@/components/SaleBusiness'
import SaleInvestment from '@/components/SaleInvestment'
import Services from '@/components/Services'
import ShiniDiski from '@/components/ShiniDiski'
import Tires from '@/components/Tires'
import New from '@/components/New'
import Avtoservis from '@/components/Avtoservis'
import Avtoservices from '@/components/Avtoservices'
import login from '@/components/cabinet/login'
import logout from '@/components/cabinet/logout'
import registration from '@/components/cabinet/registration'
import reset from '@/components/cabinet/reset'
import check from '@/components/cabinet/check'
import promocode from '@/components/cabinet/promocode'
import history from '@/components/cabinet/history'
import regcode from '@/components/cabinet/regcode'
import auth from '@/components/cabinet/auth'
import my from '@/components/cabinet/user/my'
import messages from '@/components/cabinet/messages'
import Keys from '@/components/Keys'
import unsubscribe from '@/components/unsubscribe'

import Loading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'
Vue.use(Loading)
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      alias: '/index/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/acii/',
      name: 'Stocks',
      component: Stocks
    },
    {
      path: '/all-services/',
      name: 'AllServices',
      component: AllServices
    },
    {
      path: '/all_car_services/',
      name: 'AllCarServices',
      component: AllCarServices
    },
    {
      path: '/battery/',
      name: 'Battery',
      component: Battery
    },
    {
      path: '/blog/',
      name: 'Blog',
      component: Blog
    },
    {
      path: '/bonus/',
      name: 'Bonus',
      component: Bonus
    },
    {
      path: '/car-marks-list/',
      name: 'AllMarks',
      alias: '/car-marks/',
      component: AllMarks
    },
    {
      path: '/connect-service/',
      name: 'ConnectService',
      component: ConnectService
    },
    {
      path: '/connection/',
      name: 'Connection',
      component: Connection
    },
    {
      path: '/contacts/',
      name: 'Contacts',
      component: Contacts
    },
    {
      path: '/detskie_avtokresla_i_boosteri/',
      name: 'Kresla',
      component: Kresla
    },
    {
      path: '/sale-franchise/',
      name: 'SaleFranchise',
      alias: ['/fran/', '/sale-franchise-by/', '/sale-franchise-uk/'],
      component: SaleFranchise
    },
    {
      path: '/kupit_akkumulyator/',
      name: 'Akb',
      component: Akb
    },
    {
      path: '/news/',
      name: 'News',
      component: News
    },
    {
      path: '/new/:id/',
      name: 'New',
      component: New
    },
    {
      path: '/sale-business/',
      name: 'SaleBusiness',
      component: SaleBusiness
    },
    {
      path: '/sale-investment/',
      name: 'SaleInvestment',
      component: SaleInvestment
    },
    {
      path: '/services/',
      name: 'Services',
      component: Services
    },
    {
      path: '/shini_i_diski/',
      name: 'ShiniDiski',
      component: ShiniDiski
    },
    {
      path: '/tires/',
      name: 'Tires',
      component: Tires
    },
    {
      path: '/avtoservis/',
      name: 'Avtoservices',
      component: Avtoservices
    },
    {
      path: '/avtoservis/:id',
      name: 'Avtoservis',
      component: Avtoservis
    },
    {
      path: '/cabinet/login/',
      name: 'login',
      component: login
    },
    {
      path: '/cabinet/my/',
      name: 'my',
      component: my
    },
    {
      path: '/cabinet/logout/',
      name: 'logout',
      component: logout
    },
    {
      path: '/cabinet/',
      name: 'check',
      component: check
    },
    {
      path: '/cabinet/promocode/',
      name: 'promocode',
      component: promocode
    },
    {
      path: '/cabinet/regcode/',
      name: 'regcode',
      component: regcode
    },
    {
      path: '/cabinet/history/',
      name: 'history',
      component: history
    },
    {
      path: '/cabinet/messages/',
      name: 'messages',
      component: messages
    },
    {
      path: '/cabinet/registration/',
      name: 'registration',
      component: registration
    },
    {
      path: '/cabinet/passresset/',
      name: 'reset',
      component: reset
    },
    {
      path: '/email/:email/:check_1/:check_2',
      name: 'auth',
      component: auth
    },
    {
      path: '/keys/:action?',
      name: 'Keys',
      component: Keys
    },
    {
      path: '/unsubscribe/:email?',
      name: 'unsubscribe',
      component: unsubscribe
    },
    {
      path: '*',
      name: 'CheckLink',
      component: CheckLink
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {selector: to.hash}
    } else if (savedPosition) {
      return savedPosition
    } else {
      return {x: 0, y: 0}
    }
  }
})

let loader
router.beforeEach((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
    // Start the route progress bar.
    loader = Vue.$loading.show({
      loader: 'dots',
      opacity: 0.8,
      backgroundColor: 'transparent',
      color: '#4A80C6',
      isFullPage: true
    })
  }
  next()
})

router.afterEach((to, from) => {
  loader.hide()
})
export default router
