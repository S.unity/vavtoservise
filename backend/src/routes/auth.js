const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const addrs = require('email-addresses')

const models = require('../models/index')
const tokenKey = '1a2b-3c4d-5e6f-7g8h'

//REGISTRATION
router.post('/registration', (req, res) => {
    console.log(req.body)
    const email = req.body.email
    const phone = req.body.phone
    const password = req.body.password
    const repeatPassword = req.body.repeatPassword
    const typeAccount = req.body.typeAccount
    const statusAccount = false
    const account = {
        name: req.body.name ? req.body.name : '',
        surname: req.body.surname ? req.body.surname : '',
        middle_name: req.body.middle_name ? req.body.middle_name : '',
        city: req.body.city ? req.body.city : '',
        avatar: req.body.city ? req.body.avatar : '',
        website: req.body.city ? req.body.website : '',
        auto: [
            {
                brand: req.body.brand ? req.body.brand : '',
                model: req.body.model ? req.body.model : '',
                mileage: req.body.mileage ? req.body.mileage : '',
                year: req.body.year ? req.body.year : '',
                osago: req.body.osago ? req.body.osago : '',
                kasko: req.body.kasko ? req.body.kasko : '',
                vin: req.body.vin ? req.body.vin : '',
            },
        ]
    }
    if (!email || !password || !repeatPassword) {
        return res.status(500).json({
            success: false,
            error: {
              info: 'Все поля должны быть заполнены',
              errorToFields: ['email', 'password', 'repeatPassword']
            }
        })
    } else if (password !== repeatPassword) {
        return res.status(500).json({
            success: false,
            error: {
              info: 'Пароли не совпадают',
              errorToFields: ['password', 'repeatPassword']
            }
        })
    } else if (addrs(email) === null) {
        return res.status(500).json({
            success: false,
            error: {
              info: 'Введите корректный Email адрес',
              errorToFields: ['email']
            }
        })
    } else {
        if (req.body.crm_user) {
          models.CRM_User.findOne({
            email
          })
            .then(user => {
              if (!user) {
                bcrypt.hash(password, null, null, (err, hash) => {
                  models.CRM_User.create({
                    email,
                    phone,
                    password: hash,
                    typeAccount,
                    statusAccount,
                    account,
                  }).then(user => {
                    return res.status(200).json({
                      success: true,
                      user,
                      token: jwt.sign({ id: user.id }, tokenKey)
                    })
                  }).catch(err => {
                    console.log(err)
                    res.status(500).json({
                      error: 'Ошибка, попробуйте позже',
                    })
                  })
                });
              } else {
                res.status(500).json({
                  error: {
                    info: 'Эта почта уже используется!',
                    errorToFields: ['email'],
                  }
                })
              }
            })
        } else {
          models.User.findOne({
            email
          })
            .then(user => {
              if (!user) {
                bcrypt.hash(password, null, null, (err, hash) => {
                  models.User.create({
                    email,
                    phone,
                    password: hash,
                    typeAccount,
                    statusAccount,
                    account,
                  }).then(user => {
                    return res.status(200).json({
                      success: true,
                      user,
                      token: jwt.sign({ id: user.id }, tokenKey)
                    })
                  }).catch(err => {
                    console.log(err)
                    res.status(500).json({
                      success: false,
                      error: {
                        info: 'Этот номер телефона уже используется!',
                        errorToFields: ['phone'],
                      }
                    })
                  })
                });
              } else {
                res.status(500).json({
                  success: false,
                  error: {
                    info: 'Эта почта уже используется!',
                    errorToFields: ['email'],
                  }
                })
              }
            })
        }
    }
})
//LoginIN
router.post('/login', (req, res) => {
    const email = req.body.email
    const password = req.body.password
    if (!email || !password) {
        res.json({
            success: false,
            error: {
              info: 'Все поля должны быть заполнены',
              errorToFields: ['email', 'password']
            }
        })
    } else {
        models.CRM_User.findOne({
            email
        }).then(user => {
            if (!user) {
                models.User.findOne({
                  email
                })
                  .then(user => {
                    if (!user) res.status(500).json({
                      success: false,
                      error: {
                        info: 'Логин или пароль неверны',
                        errorToFields: ['email', 'password']
                      }
                    })
                    else {
                      bcrypt.compare(password, user.password, function(err, result) {
                        if (!result) {
                          res.status(500).json({
                            success: false,
                            error: {
                              info: 'Логин или пароль неверны',
                              errorToFields: ['email', 'password']
                            }
                          })
                        } else {
                          return res.status(200).json({
                            success: true,
                            user,
                            token: jwt.sign({ id: user.id }, tokenKey)
                          })
                        }
                      })
                    }
                  })
            } else {
                bcrypt.compare(password, user.password, function(err, result) {
                    if (!result) {
                        res.status(500).json({
                            success: false,
                            error: {
                              info: 'Логин или пароль неверны',
                              errorToFields: ['email', 'password']
                            }
                        })
                    } else {
                        return res.status(200).json({
                            success: true,
                            user,
                            token: jwt.sign({ id: user.id }, tokenKey)
                        })
                    }
                })
            }
        }).catch(err => {
            console.log(err)
            res.status(500).json({
              success: false,
              error: {
                info: 'Server side error',
                error: err
              }
            })
        })
    }
})

module.exports = router
