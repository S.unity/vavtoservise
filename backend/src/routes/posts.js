const express = require('express')
const app = express()
const router = express.Router()
const Post = require('../models/post-model')
const fileUpload = require('express-fileupload')
const upload = require('./order')

app.use(fileUpload({}));

router.post('/api/users', (req, res) => {
    // console.log(req)
    const post = new Post({
        description: req.body.data.description,
        image: req.body.data.image,
        name: req.body.data.name,
        price: req.body.data.price,
        article: req.body.data.vendor,
        available: req.body.data.available,
        category: req.body.data.category
    })
    post.save((err, data) => {
        if (err) {
            console.log(err)
        } else {
            res.send({
                success: true,
                message: `Post with ID_${data._id} saved successfully!`
            })
        }
    })
})

router.get('/api/users', (req, res) => {
    Post.find({}, 'name description image price vendor available category', (err, posts) => {
        if (err) {
            res.sendStatus(500)
        } else {
            res.send({
                posts: posts
            })
        }
    }).sort({
        _id: -1
    })
})

router.get('/api/users/:id', (req, res) => {
    Post.findById(req.params.id, 'title description', (err, post) => {
        if (err) {
            res.sendStatus(500)
        } else {
            res.send(post)
        }
    })
})

router.put('/v1/users/:id', (req, res) => {
    Post.findById(req.params.id, 'name description image price vendor available category', (err, post) => {
        if (err) {
            console.log(err)
        } else {
            if (req.body.name) {
                post.name = req.body.name
            }
            if (req.body.description) {
                post.description = req.body.description
            }
            if (req.body.image) {
                post.image = req.body.image
            }
            if (req.body.price) {
                post.price = req.body.price
            }
            if (req.body.vendor) {
                post.vendor = req.body.vendor
            }
            if (req.body.available) {
                post.available = req.body.available
            }
            if (req.body.category) {
                post.category = req.body.category
            }
            post.save(err => {
                if (err) {
                    res.sendStatus(500)
                } else {
                    res.sendStatus(200)
                }
            })
        }
    })
})

router.delete('/v1/posts/:id', (req, res) => {
    Post.remove({
        _id: req.params.id
    }, err => {
        if (err) {
            res.sendStatus(500)
        } else {
            res.sendStatus(200)
        }
    })
})

module.exports = router
