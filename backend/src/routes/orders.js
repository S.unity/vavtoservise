const express = require('express')
const router = express.Router()

const jwt = require('jsonwebtoken')
const User = require('../models/user')
const tokenKey = '1a2b-3c4d-5e6f-7g8h'

router.get('/v1/orders', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      async (err, payload) => {
        if (err) next()
        else if (payload) {
          await User.findOne({
            _id: payload.id
          }).then(user => {
            res.json({
              orders: user.orders,
              ok: true
            })
            if (!user) res.json({
              ok: false,
              comment: 'У не зарегистрированно пользователя не может быть истории заказов'
            })
          }).catch((err)=>{
            res.json({
              ok: false,
              error: err,
              comment: 'У не зарегистрированно пользователя не может быть истории заказов'
            })
          })
        }
      }
    )
  }
})

module.exports = router
