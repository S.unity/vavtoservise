const auth = require('./auth')
const search = require('./search')
const action = require('./action')

module.exports = {
    auth,
    search,
    action
}
