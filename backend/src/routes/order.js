const express = require('express')
const router = express.Router()
const multer = require('multer')
const path = require('path')
const Sharp = require('sharp')
const mkdirp = require('mkdirp')
const diskStorage = require('../util/diskStorage')
const User = require('../models/user')
const jwt = require('jsonwebtoken')
const tokenKey = '1a2b-3c4d-5e6f-7g8h'

let product

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const pathname = req.body.pathname + '_' + req.body.size
        mkdirp.sync(`../uploads/${pathname}`)
        cb(null, `../uploads/${pathname}`)
    },
    filename: (req, file, cb) => {
        let quantity
        if (typeof req.body.quantity === 'object') quantity = req.body.quantity.pop()
        else quantity = req.body.quantity
        cb(null, quantity + '_' + Date.now() + path.extname(file.originalname))
    },
    sharp: async (req, file, cb) => {
        const resizer =
          await Sharp()
            .resize(1024, 768)
            .max()
            .withoutEnlargement()
            .toFormat('jpg')
            .jpeg({
                quality: 40,
                progressive: true
            });
        cb(null, resizer)
    }
})
const order = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        const ext = path.extname(file.originalname)
        if (ext !== '.jpg' && ext !== '.JPG' && ext !== '.jpeg' && ext !== '.png') {
            const err = new Error('Extention')
            err.code = 'EXTENTION'
            return cb(err)
        }
        cb(null, true)
    }
}).array('file')


router.post('/v1/order', async (req, res, next) => {
    order(req, res, async function (err) {
        let error = ''
        if (err) {
            if (err.code === 'LIMIT_FILE_SIZE') {
                error = 'Картинка неболее 2мб'
            }
            if (err.code === 'EXTENTION') {
                error = 'Только картинка'
            }
        }
        // получаем токен, декодируем и получаем id, ищем по id пользователя
        // когда нашли добавляем в пользовтеля новый обьект orders
        // в orders хранится тип, количество, цена и время заказа, а product хранит имена файлов
        if (req.headers.authorization) {
            jwt.verify(
              req.headers.authorization,
              tokenKey,
              (err, payload) => {
                  if (err) next()
                  else if (payload) {
                      let imageName = []
                      req.files.forEach(el => imageName.push({ name: el.originalname, quantity: Number(req.body.quantity.join()), size: req.body.size }))
                      User.updateOne(
                        { "_id": payload.id},
                        // Доки по $push - https://docs.mongodb.com/manual/reference/operator/update/push/
                        {$push: {
                            orders: {
                                $each: [ {
                                    id: req.body.pathname,
                                    time: new Date().toLocaleDateString('ru-RU'),
                                    price: req.body.price,
                                    order_length: req.body.order_length,
                                    product: {
                                        photos: imageName,
                                        product: JSON.parse(req.body.products)
                                    }
                                } ]
                            }
                            }}
                      )
                        .then((obj) => {
                          console.log('success save!')
                      })
                        .catch((err) => {
                            console.log('Error: ' + err)
                        })
                  }
              }
            )
        }
        await res.json({
            order: req.body.pathname,
            ok: !error,
            // path: req.file.path,
            error: error
        })
    })
})

module.exports = router
