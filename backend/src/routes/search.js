const express = require('express')
const router = express.Router()
// const Post = require('../models/post-model')
const Users = require('../models/user')
const CRM_User = require('../models/crm-user')
const jwt = require("jsonwebtoken");
const Tasks = require("../models/tasks");
const Applications = require("../models/applications");
const tokenKey = '1a2b-3c4d-5e6f-7g8h'


router.get('/get-users', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          CRM_User.findOne({
            _id: payload.id
          }).then(user => {
            if (user) {
              if (user.email === 'rn.09@bk.ru') {
                Users.find()
                  .then(users => {
                    res.status(200).send({
                      users
                    })
                  })
              } else {
                return res.status(500).json({
                  success: false,
                  message: 'Administrator rights required'
                })
              }
            }
            if (!user) return res.status(500).json({
              success: false,
              message: 'Administrator rights required'
            })
          }).catch((err)=>{
            console.log(err)
            return res.status(500).json({
              success: false,
              message: 'Server side error',
              error: err
            })
          })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})
router.get('/get-crm-users', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          CRM_User.findOne({
            _id: payload.id
          }).then(user => {
            if (user) {
              if (user.email === 'rn.09@bk.ru') {
                CRM_User.find()
                  .then(users => {
                    const index = users.findIndex(({ id }) => id === user.id)
                    if (index >= 0) users.splice(index, 1)
                    res.status(200).send({
                      users
                    })
                  })
              } else {
                return res.status(500).json({
                  success: false,
                  message: 'Administrator rights required'
                })
              }
            }
            if (!user) return res.status(500).json({
              success: false,
              message: 'Administrator rights required'
            })
          }).catch((err)=>{
            console.log(err)
            return res.status(500).json({
              success: false,
              message: 'Server side error',
              error: err
            })
          })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})
router.get('/get-tasks', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          CRM_User.findOne({
            _id: payload.id
          }).then(user => {
            if (user) {
              Tasks.find()
                .then(tasks => {
                  console.log(tasks)
                  if (user.email !== 'rn.09@bk.ru') {
                    tasks = tasks.filter(el => {
                      if (el.visibility.id === user.id || el.visibility.id.toLowerCase() === 'всем') return el
                    })
                    res.status(200).send({
                      tasks
                    })
                  }
                  else res.status(200).send({
                    tasks
                  })
                })
            }
            else return res.status(500).json({
              success: false,
              message: 'Administrator rights required'
            })
          }).catch((err)=>{
            console.log(err)
            return res.status(500).json({
              success: false,
              message: 'Server side error',
              error: err
            })
          })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})
router.get('/get-application', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          Users.findOne({
            _id: payload.id
          }).then(user => {
            console.log(user)
            if (user) {
              Applications.find()
                .then(application => {
                  application = application.filter((el) => {
                    if (el.owner.id == user._id) return el
                  })
                  res.status(200).send({
                    application
                  })
                })
            } else return res.status(500).json({
              success: false,
              message: 'Administrator rights required'
            })
          }).catch((err)=>{
            console.log(err)
            return res.status(500).json({
              success: false,
              message: 'Server side error',
              error: err
            })
          })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})

// вся эта катавасия оказалась не нужной, ведь все я сделал в фронте

// router.post('/v1/posts/search', (req, res) => {
//   console.log(req.body)
//   Post.find({ $or: [{name: {$regex: "красавица", $options: "$i"}}, {description: {$regex: "красавица", $options: "$i"}}] } ).then((post, err) => {
//     console.log(post)
//     console.log(err)
//     res.send({
//       posts: post
//     })
//   })
// })

module.exports = router
