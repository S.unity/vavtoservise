const express = require('express')
const router = express.Router()
const User = require('../models/user')
const Applications = require('../models/applications')
const CRM_User = require('../models/crm-user')
const Tasks = require('../models/tasks')
const jwt = require("jsonwebtoken")
const tokenKey = '1a2b-3c4d-5e6f-7g8h'

router.put('/confirm-status-user', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          CRM_User.findOne({
            _id: payload.id
          }).then(user => {
            if (user) {
              if (user.email === 'rn.09@bk.ru') {
                User.findById(req.body._id,(err, user) => {
                  if (err) {
                    console.log(err)
                    return res.status(500).json({
                      success: false,
                      message: 'Administrator rights required',
                      error: err
                    })
                  } else {
                    user.statusAccount = true
                    user.save(err => {
                      if (err) {
                        res.sendStatus(500)
                      } else {
                        res.sendStatus(200)
                      }
                    })
                  }
                })
              } else {
                return res.status(500).json({
                  success: false,
                  message: 'Administrator rights required'
                })
              }
            }
            if (!user) return res.status(500).json({
              success: false,
              message: 'Administrator rights required'
            })
          }).catch((err)=>{
            console.log(err)
            return res.status(500).json({
              success: false,
              message: 'Server side error',
              error: err
            })
          })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})

router.post('/add-new-task', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          CRM_User.findOne({
            _id: payload.id
          }).then(user => {
            if (user) {
              if (user.email === 'rn.09@bk.ru') {
                const post = new Tasks({
                  id: req.body.id,
                  title: req.body.title,
                  subtitle: req.body.subtitle ? req.body.subtitle : '',
                  content: req.body.content,
                  date: req.body.date,
                  visibility: req.body.visibility
                })
                post.save((err, data) => {
                  if (err) {
                    console.log(err)
                    res.status(500).json({
                      success: false,
                      message: err
                    })
                  } else {
                    res.status(200).json({
                      success: true,
                      data: data,
                      message: 'Add task successful'
                    })
                  }
                })
              } else {
                return res.status(500).json({
                  success: false,
                  message: 'Administrator rights required'
                })
              }
            }
            if (!user) return res.status(500).json({
              success: false,
              message: 'Administrator rights required'
            })
          }).catch((err)=>{
            console.log(err)
            return res.status(500).json({
              success: false,
              message: 'Server side error',
              error: err
            })
          })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})
router.delete('/delete-task', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          CRM_User.findOne({
            _id: payload.id
          }).then(user => {
            if (user) {
              if (user.email === 'rn.09@bk.ru') {
                console.log(req.body._id)
                Tasks.remove({
                  _id: req.body._id
                })
                  .then(() => {
                    return res.status(200).json({
                      success: true,
                      message: 'Task deleted successfully'
                    })
                  })
                  .catch(err => {
                    return res.status(500).json({
                      success: false,
                      message: 'Server side error. Perhaps the object was not found in the database',
                      error: err
                    })
                  })
              } else {
                return res.status(500).json({
                  success: false,
                  message: 'Administrator rights required'
                })
              }
            }
            if (!user) return res.status(500).json({
              success: false,
              message: 'Administrator rights required'
            })
          }).catch((err)=>{
            console.log(err)
            return res.status(500).json({
              success: false,
              message: 'Server side error',
              error: err
            })
          })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})

router.post('/new-application', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          User.findById(req.body._id,(err, user) => {
            if (err) {
              console.log(err)
              return res.status(500).json({
                success: false,
                message: 'Authorization required',
                error: err
              })
            } else {
              const application = new Applications({
                owner: req.body.owner,
                city: req.body.city,
                status: false,
                realization_status: req.body.realization_status,
                info: req.body.info,
                offers: req.body.offers,
              })
              application.save(err => {
                if (err) {
                  res.status(500).json({
                    success: true,
                    message: 'error server',
                    error: err
                  })
                } else {
                  res.status(200).json({
                    success: true,
                    message: 'application added',
                  })
                }
              })
            }
          })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})

router.post('/update-profile', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization,
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          User.findById(req.body._id,(err, user) => {
            if (err) {
              console.log(err)
              return res.status(500).json({
                success: false,
                message: 'Authorization required',
                error: err
              })
            } else {
              User.updateOne(
                { "_id": req.body._id},
                // Доки по $push - https://docs.mongodb.com/manual/reference/operator/update/push/
                {
                  $set: {
                    email: req.body.email,
                    phone: req.body.phone,
                    ['account.name']: req.body.name,
                    ['account.surname']: req.body.surname,
                    ['account.middle_name']: req.body.middle_name
                  },
                },
              )
                .then((obj) => {
                  console.log(obj)
                  return res.status(200).json({
                    success: true,
                    message: 'Successfully save',
                  })
                })
                .catch((err) => {
                  return res.status(500).json({
                    success: false,
                    message: 'Error update',
                    error: err
                  })
                })
            }
          })
            .catch((err) => {
              return res.status(500).json({
                success: false,
                message: 'User not search',
                error: err
              })
            })
        }
      }
    )
  } else return res.status(500).json({
    success: false,
    message: 'Authorization required',
  })
})

module.exports = router
