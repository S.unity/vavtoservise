const User = require('./user')
const CRM_User = require('./crm-user')
const Orders = require('./orders')
const Task = require('./tasks')
module.exports = {
    User,
    Orders,
    CRM_User,
    Task
}
