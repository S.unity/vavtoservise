const mongoose = require('mongoose')
const Schema = mongoose.Schema
const PostSchema = new Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    image: {
        type: Array
    },
    price: {
        type: Number
    },
    vendor: {
        type: String,
        unique: true
    },
    available: {
        type: String
    },
    category: {
        type: String
    }
})
const PostModel = mongoose.model('posts', PostSchema)
module.exports = PostModel
