const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema(
  {
    // Информация о владельце зявки (id, имя, телефон, комментарий)
    owner: {
      type: Object,
      required: true,
    },
    // Город владельца зявки
    city: {
      type: String,
      required: true,
    },
    // Статус заявки, утверждена или нет
    status: {
      type: Boolean,
      required: true,
    },
    // На какой статии реализации заявка
    realization_status: {
      type: Object,
      required: true,
    },
    // Информация о заявке: дата, авто, год выпуска, vin, какая услуга, комментарий
    info: {
      type: Object,
      required: true,
    },
    // Предложения от СТО к этой заявке
    offers: [
      {
        // Информация о СТО, которое делает предложение
        performer: {
          type: String,
          required: true,
        },
        // Цена предложения
        price: {
          type: String,
          required: true,
        },
        // Комментарий от СТО
        comments: {
          type: String,
          required: true,
        }
      }
    ],
  },
  {
    timestamps: true
  }
)
schema.set('toJSON', {
  virtuals: true
})

module.exports = mongoose.model('Applications', schema)
