const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema(
  {
    id: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: false,
    },
    subtitle: {
        type: String,
        required: false,
    },
    content: {
        type: String,
        required: false,
    },
    date: {
        type: String,
        required: true,
    },
    visibility: {
      type: Array,
      required: true,
    }
  },
  {
    timestamps: true
  }
)
schema.set('toJSON', {
    virtuals: true
})

module.exports = mongoose.model('Tasks', schema)
