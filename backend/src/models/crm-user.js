const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema(
  {
    email: {
        type: String,
        required: false,
        unique: true
    },
    name: {
        type: String,
        required: false,
    },
    phone: {
        type: String,
        required: false,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    typeAccount: {
      type: String,
      required: true,
    },
    statusAccount: {
      type: Boolean,
      required: true,
    },
    account: {
      type: Object,
      required: true,
    }
  },
  {
    timestamps: true
  }
)
schema.set('toJSON', {
    virtuals: true
})

module.exports = mongoose.model('CRM_User', schema)
