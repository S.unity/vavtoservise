const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema({
    orderId: {
      type: String,
      unique: true
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId
    },
    products: [{
      type: Object,
    }]
  },
  {
    timestamps: true
  }
)
// schema.set('toJSON', {
//   virtuals: true
// })

module.exports = mongoose.model('Orders', schema)
