const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const mongoose = require('mongoose')
const config = require('./config/config')
const routes = require('./routes/index')
const https = require('https')
const fs = require('fs')
const path = require('path')
const keyPath = path.join(__dirname, '/path/privkey.pem')
const certPath = path.join(__dirname, '/path/cert.pem')
const options = {
    key: fs.readFileSync(keyPath),
    cert: fs.readFileSync(certPath)
}

const jwt = require('jsonwebtoken')

const models = require('./models/index')
const tokenKey = '1a2b-3c4d-5e6f-7g8h'
// const session = require('express-session');

// const MongoStore = require('connect-mongo').default;

mongoose.Promise = global.Promise;

const app = express()

app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

app.use('/', routes.auth)
app.use('/', routes.search)
app.use('/', routes.action)
app.use((req, res, next) => {
    if (req.headers.authorization) {
        jwt.verify(
          req.headers.authorization,
          tokenKey,
          (err, payload) => {
              if (err) next()
              else if (payload) {
                  models.User.findOne({
                      _id: payload.id
                  }).then(user => {
                      console.log(user)
                      if (user) {
                          if (user.email === 'rn.09@bk.ru') {
                              return res.json({
                                  ok: true,
                                  admin: true
                              })
                          } else {
                              return res.json({
                                  ok: true,
                              })
                          }
                      }
                      if (!user) console.log(user, 'нет таких')
                  }).catch((err)=>{
                      console.log(err)
                  })
              }
          }
        )
    }
})
app.use(function(req, res, next){
    const err = new Error('Ни хрена не найдено!');
    err.status = 404;
    next(err);
});

mongoose.connect(config.dbURL, config.dbOptions)
// mongoose.connection
//     .once('open', () => {
//         console.log(`Mongoose - successful connection ...`)
//         https.createServer(options, app).listen(process.env.PORT || config.port,
//             () => console.log(`Сервер пашет на порту: ${config.port} ...`))
//     })
//     .on('error', error => console.warn(error))
mongoose.connection
    .once('open', () => {
        console.log(`Mongoose - successful connection ...`)
        app.listen(process.env.PORT || config.port,
          () => console.log(`Сервер пашет на порту: ${config.port} ...`))
    })
    .on('error', error => console.warn(error))
